class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.date :due_date
      t.boolean :active
      t.references :accomodation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
