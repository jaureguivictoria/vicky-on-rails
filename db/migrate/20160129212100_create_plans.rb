class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :month_duration
      t.float :cost

      t.timestamps null: false
    end
  end
end
