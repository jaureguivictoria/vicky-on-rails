class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.date :date
      t.float :amount
      t.string :responsible
      t.references :plan, index: true, foreign_key: true
      t.references :payment_type, index: true, foreign_key: true
      t.references :ad, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
