# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160131132935) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accomodation_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "accomodations", force: :cascade do |t|
    t.string   "name"
    t.integer  "accomodation_type_id"
    t.integer  "client_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "accomodations", ["accomodation_type_id"], name: "index_accomodations_on_accomodation_type_id", using: :btree
  add_index "accomodations", ["client_id"], name: "index_accomodations_on_client_id", using: :btree

  create_table "ads", force: :cascade do |t|
    t.date     "due_date"
    t.boolean  "active"
    t.integer  "accomodation_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "ads", ["accomodation_id"], name: "index_ads_on_accomodation_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payments", force: :cascade do |t|
    t.date     "date"
    t.float    "amount"
    t.string   "responsible"
    t.integer  "plan_id"
    t.integer  "payment_type_id"
    t.integer  "ad_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "payments", ["ad_id"], name: "index_payments_on_ad_id", using: :btree
  add_index "payments", ["payment_type_id"], name: "index_payments_on_payment_type_id", using: :btree
  add_index "payments", ["plan_id"], name: "index_payments_on_plan_id", using: :btree

  create_table "plans", force: :cascade do |t|
    t.integer  "month_duration"
    t.float    "cost"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_foreign_key "accomodations", "accomodation_types"
  add_foreign_key "accomodations", "clients"
  add_foreign_key "ads", "accomodations"
  add_foreign_key "payments", "ads"
  add_foreign_key "payments", "payment_types"
  add_foreign_key "payments", "plans"
end
