class Payment < ActiveRecord::Base
  has_one :plan
  has_one :payment_type
  belongs_to :ad
end
