# Vicky On Rails #

This repository contains a simple CRUD project I started for learning Rails basics. I used Postgresql, Heroku, Active Record, Bootstrap, and others.

The [class diagram ](https://bitbucket.org/jaureguivictoria/vickyonrails/src/241871e1ce59965e50b808520facf24a9aab753a/class_diagram.png?at=master)contains the systems structure and expected behaviour.